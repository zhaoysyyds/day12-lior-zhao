import { useState } from "react";
import { useDispatch } from "react-redux";
import { onAdd } from "./todoSlice";

const TodoItemGenerator = (props) => {


    const [content, setContent] = useState('');
    const dispatch = useDispatch();

    const handleChangeInput = (e) => {
        setContent(e.target.value);
    }

    const handleChange = () => {
        if (content.trim() === '') return;
        const todo = {};
        todo.id = (new Date()).toString();
        todo.text = content;
        todo.done = false;
        dispatch(onAdd(todo));

        setContent('');
    }

    return (
        <div>
            <input type="text" value={content} onChange={handleChangeInput}></input>
            <button onClick={handleChange}>add</button>
        </div>
    )
}

export default TodoItemGenerator;

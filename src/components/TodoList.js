import TodoGroup from "./TodoGroup";
import TodoItemGenerator from "./TodoItemGenerator";
import "./../style/todolist.css";
import { useSelector } from "react-redux";

const TodoList = () => {
  const todoList = useSelector((state) => state.todoList.todoList);

  return (
    <div>
      <p className="todo-list">Todo List</p>
      <TodoGroup todoList={todoList}></TodoGroup>
      <TodoItemGenerator></TodoItemGenerator>
    </div>
  );
};

export default TodoList;

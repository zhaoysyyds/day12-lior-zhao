import { createSlice } from "@reduxjs/toolkit";

export const todoSlice = createSlice({
    name: "todo",
    initialState: {
        todoList: [
            {
                id: "1111",
                text: "todo example",
                done: false
            },
            {
                id: "112222",
                text: "first todo example",
                done: false
            }
        ]
    },
    reducers: {
        onAdd: (state, action) => {
            state.todoList.push(action.payload);
        },
        updateDone: (state,action)=>{
            let findTodo=state.todoList.find(todo=>todo.id===action.payload)
            findTodo.done=!findTodo.done;
        },
        deleteTodo: (state,action)=>{
            state.todoList=state.todoList.filter(todo=>todo.id!==action.payload)
        }
    }
});
export const { onAdd ,updateDone,deleteTodo} = todoSlice.actions;
export default todoSlice.reducer;